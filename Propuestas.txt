1. Base de datos de la peluquería “Hair Up”:
•Se desea desarrollar un sistema de información para gestionar los empleados, los clientes, productos, cortes de pelo, proveedores y citas de la peluquería “Hair Up”.
•Los clientes eligen un solo tipo de peinado, pero un peinado puede ser utilizado por varios clientes.
•De los clientes se desea almacenar el DNI, nombre, apellidos, fecha de nacimiento, dirección, teléfono y sexo. Hay que tener en cuenta que un cliente puede tener varios números de teléfono y tener dos o más direcciones diferentes.
•De los cortes de pelo se desea almacenar el tipo de peinado, el color de tinte y el tipo de tratamiento necesario (solo corte, corte y lavado, etc.).
•Los clientes solicitan una cita a los empleados. Un cliente puede pedir varias citas, pero una cita pertenece a un solo cliente, por lo que será necesario almacenar la fecha y la hora de la cita, y el DNI del cliente.
•De los empleados se quiere conocer su DNI, nombre, apellidos, cargo, antigüedad y salario. Un empleado atiende a un solo cliente, pero un cliente puede ser atendido por varios empleados.
•Los empleados pueden utilizar diferentes productos para hacer cada tratamiento, por lo que de estos interesa conocer el código de producto, nombre y marca. Un empleado puede usar varios productos para hacer un corte de pelo, y un mismo producto puede ser utilizado por varios empleados.
•Un proveedor suministra los productos a la empresa. De estos se quiere conocer su DNI, nombre, apellidos, teléfono, empresa con la que trabaja y su dirección. Un producto solo puede ser suministrado por un proveedor, y un proveedor suministra varios productos diferentes.

2. Base de datos de la agencia inmobiliaria “Your Home”:
•Se desea desarrollar un sistema de información para gestionar los empleados, los clientes y los inmuebles a la venta y de alquiler de la agencia inmobiliaria “Your Home”.
•Los clientes que vienen a la agencia buscan comprar o alquilar un inmueble. No hay límites, por lo que un cliente puede comprar o alquilar varios inmuebles y varios clientes pueden también comprar o alquilar un solo inmueble. De estos clientes se desea almacenar el DNI, nombre, apellidos, fecha de nacimiento, situación laboral (estudiante, empleado, empresario…) y teléfono.
•De los inmuebles en alquiler o en venta se desea saber el código, precio, tamaño, tipo, nº de habitaciones, nº de baños, equipamiento (si está amueblado), estado, características y nº de planta (en caso de que sea un piso). Evidentemente, se desea saber también el tipo de régimen (en venta o en alquiler).
•Para la agencia inmobiliaria trabajan varios empleados. De estos se desea saber su DNI, nombre, apellidos, cargo, antigüedad y salario. Un empleado puede atender a varios clientes y un cliente puede ser atendido por varios empleados.

3. Base de datos del pool-bar “Dutchman”:
•Se desea desarrollar un sistema de información para gestionar los camareros, los clientes, las facturas, las ventas, las mesas y los pedidos del pool-bar “Dutchman”.
•De los clientes se desea saber el DNI, nombre, apellidos, dirección, teléfono, fecha de nacimiento y email.
•Los camareros atienden a los clientes. Un camarero puede atender a varios clientes, pero un cliente solo puede ser atendido por un camarero. De los camareros de desea saber su DNI, nombre, apellidos, dirección, teléfono y nº de turno.
•Los clientes suelen hacer pedidos. Un pedido solo puede corresponder a un cliente, pero un cliente puede hacer varios pedidos. De los pedidos se desea almacenar su id, tipo de bebida, tipo de alimento (pizza, hamburguesa, etc.) y tipo de aperitivo (patatas, Nuggets, fingers…).
•Un cliente suele ocupar una mesa de billar. Un cliente solo puede ocupar una mesa, y una mesa solo puede ser ocupada por un cliente. De las mesas queremos saber su id, si es de fumadores o no, el tipo (si es de billar, domino o futbolín), las horas y los minutos que un cliente pasa en una mesa.
•Cuando un cliente paga la venta de su pedido, puede a la vez solicitar una factura de esa misma venta. Como un cliente puede hacer varios pedidos, puede pagar varias ventas y solicitar varias facturas, pero a cada venta solo le corresponde una factura, por lo que un venta solo puede tener una factura. Una factura le corresponde a un único cliente, a la vez que una venta le corresponde a un solo cliente.
•De las ventas queremos saber su código, la cantidad en euros del pedido, su forma de pago (efectivo o tarjeta), el número de mesa y los productos del pedido; mientras que de las facturas queremos saber su id, la fecha, los productos consumidos, su valor y la cantidad de productos.

4. Base de datos del restaurante-marisquería “El cabaret”:
•Se desea desarrollar un sistema de información para gestionar los camareros, los clientes, las mesas, los platos y las cuentas del restaurante-marisquería “El cabaret”.
•De los camareros se desea saber su DNI, nombre, apellidos, cargo, antigüedad y salario. Un camarero atiende a un solo cliente, y un cliente solo puede ser atendido por un camarero. De los clientes se desea saber el DNI, nombre, apellidos y fecha de nacimiento.
•Un cliente solo puede ocupar una mesa, pero una mesa puede ser ocupada por varios clientes. De las mesas queremos conocer la capacidad (número de clientes máximo en esa mesa), su número de mesa y su ubicación (si es de terraza o de interior).
•Respecto a los platos, un cliente puede pedir varios platos y un mismo plato puede ser pedido por varios clientes. De los platos queremos conocer su id, su tipo (primero, segundo, postre…) y su precio.
•Al acabar, un cliente puede solicitar una sola cuenta de su pedido. Hay que tener en cuenta que un pedido corresponde a una mesa, por lo que puede corresponder a varios clientes. De las cuentas queremos saber su código, su cuantía y el número de mesa. Hay que tener en cuenta que una mesa puede tener varios pedidos en el mismo día, por lo que es importante saber la fecha de la cuenta.

5. Base de datos del supermercado “PlusMarket”:
•Se desea desarrollar un sistema de información para gestionar los empleados, las compras, los productos, las ventas, los cargos, las marcas, los proveedores y los departamentos del supermercado “PlusMarket”.
•De los empleados que trabajan en el supermercado queremos saber su id, email, nombre, apellidos, dirección, clave, teléfono y tipo de empleado. Hay que tener en cuenta que un empleado puede tener varias direcciones y varios números de teléfono.
•Cada empleado tiene un cargo en el supermercado. De esto queremos saber el tipo de cargo y su descripción. Hay que tener en cuenta que cada empleado solo tiene un cargo, pero varios pueden tener el mismo cargo.
•Un empleado realiza una venta. Hay que tener en cuenta que cada venta corresponde a un empleado, pero un empleado puede realizar varias ventas. De las ventas queremos saber su id, fecha, precio y descuento.
•Al igual que un empleado realiza una venta, también realiza una compra. Cada compra pertenece a un empleado y un empleado puede realizar varias compras. De las compras queremos conocer su id, fecha y precio.
•Las ventas y las compras pertenecen cada una a un producto. Una misma compra y una misma venta puede tener diferentes productos, y a la vez un producto puede haber sido utilizado en varias ventas y en varias compras, por lo que es importante saber la cantidad de cada producto y el precio de cada unidad. De los productos queremos conocer su id, nombre, precio y unidades en stock.
•Cada producto tiene su marca. Un producto puede ser vendido por diferentes marcas y una marca puede vender diferentes productos. De las marcas queremos conocer su id y nombre.
•Los proveedores proveen cada uno una marca determinada. A tener en cuenta que una marca solo puede ser provista por un solo proveedor, pero un proveedor puede distribuir diferentes marcas. De los proveedores queremos conocer su id, nombre, apellidos, dirección y teléfono. Hay que tener en cuenta que un proveedor puede tener varias direcciones y varios números de teléfono.
•Por último, cada producto pertenece a un departamento. Cada producto solo pertenece a un departamento, pero un departamento puede tener varios productos. De estos departamentos queremos conocer su id, nombre y descripción.
